package nix.autoframework1.spec

import geb.spock.GebReportingSpec
import nix.autoframework1.page.BlogPage
import nix.autoframework1.page.StartPage

class NixNavigationSpec extends GebReportingSpec{

    def navigateToMainPage(){
        when:
       to StartPage

        and:
        "User navigates to Blog page"()

        then:
       at BlogPage
    }
}
